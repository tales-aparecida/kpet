# Copyright (c) 2021 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for host type filtering"""
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_test_list
from tests.test_integration import kpet_with_db

OUTPUT_TXT_J2 = """
    {%- for scene in SCENES -%}
        {{- "SCENE:" -}}
        {%- for recipeset in scene.recipesets -%}
            {{- "RECIPESET:" -}}
            {%- for host in recipeset -%}
                {{- "HOST:" -}}
                {%- for test in host.tests -%}
                    {{- "TEST:" + test.name -}}
                {%- endfor -%}
            {%- endfor -%}
        {%- endfor -%}
    {%- endfor -%}
"""


class IntegrationDomainsTests(IntegrationTests):
    """Integration tests for host domain handling"""

    def test_no_arches(self):
        """Check host types without arches are not filtered"""
        common_index_yaml = """
                recipesets:
                    rcs1:
                      - a
                      - b
                arches:
                    - arch1
                    - arch2
                    - arch3
                trees:
                    tree:
                        arches: .*
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
        """
        assets_without_domains = {
            "index.yaml": """
                host_types:
                    a: {}
                    b: {}
            """ + common_index_yaml,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        assets_with_domains = {
            "index.yaml": """
                domains:
                    general:
                        description: Generally-available hosts
                host_types:
                    a:
                        domains: general
                    b:
                        domains: general
            """ + common_index_yaml,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        for assets in (assets_without_domains, assets_with_domains):
            with assets_mkdir(assets) as db_path:
                for arch in ("arch1", "arch2", "arch3"):
                    self.assertKpetProduces(
                        kpet_with_db, db_path,
                        "--debug", "run", "generate",
                        "-t", "tree", "-a", arch,
                        "-k", "kernel.tar.gz", "--no-lint",
                        stdout_equals='SCENE:RECIPESET:'
                                      'HOST:TEST:Test1'
                                      'HOST:TEST:Test2'
                    )
                    self.assertKpetProduces(
                        kpet_test_list, db_path,
                        "-a", arch,
                        stdout_equals='Test1\nTest2\n'
                    )

    def test_arches(self):
        """Check host types with arches are appropriately filtered"""
        common_index_yaml = """
                recipesets:
                    rcs1:
                      - a
                      - b
                      - c
                arches:
                    - arch1
                    - arch2
                    - arch3
                    - arch4
                trees:
                    tree:
                        arches: .*
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
                        test1:
                            name: Test1
                            host_types: a
                        test2:
                            name: Test2
                            host_types: b
                        test3:
                            name: Test3
                            host_types: c
        """

        assets_without_domains = {
            "index.yaml": """
                host_types:
                    a:
                        arches: arch[123]
                    b:
                        arches: arch[23]
                    c:
                        arches:
                            - arch[3]
            """ + common_index_yaml,
            "output.txt.j2": OUTPUT_TXT_J2,
        }

        with assets_mkdir(assets_without_domains) as db_path:
            self.assertKpetProduces(
                kpet_with_db, db_path,
                "--debug", "run", "generate",
                "-t", "tree", "-a", "arch1",
                "-k", "kernel.tar.gz", "--no-lint",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST:TEST:Test1'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch1",
                stdout_equals='Test1\n'
            )

            self.assertKpetProduces(
                kpet_with_db, db_path,
                "--debug", "run", "generate",
                "-t", "tree", "-a", "arch2",
                "-k", "kernel.tar.gz", "--no-lint",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST:TEST:Test1'
                              'HOST:TEST:Test2'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch2",
                stdout_equals='Test1\nTest2\n'
            )

            self.assertKpetProduces(
                kpet_with_db, db_path,
                "--debug", "run", "generate",
                "-t", "tree", "-a", "arch3",
                "-k", "kernel.tar.gz", "--no-lint",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST:TEST:Test1'
                              'HOST:TEST:Test2'
                              'HOST:TEST:Test3'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch3",
                stdout_equals='Test1\nTest2\nTest3\n'
            )

            self.assertKpetProduces(
                kpet_with_db, db_path,
                "--debug", "run", "generate",
                "-t", "tree", "-a", "arch4",
                "-k", "kernel.tar.gz", "--no-lint",
                stdout_equals='SCENE:'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch4",
                stdout_equals=''
            )

            self.assertKpetProduces(
                kpet_test_list, db_path,
                stdout_equals='Test1\nTest2\nTest3\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", ".*",
                stdout_equals='Test1\nTest2\nTest3\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch[12]",
                stdout_equals='Test1\nTest2\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch[23]",
                stdout_equals='Test1\nTest2\nTest3\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch[34]",
                stdout_equals='Test1\nTest2\nTest3\n'
            )

        assets_with_domains = {
            "index.yaml": """
                domains:
                    X:
                        description: Hosts X
                    Y:
                        description: Hosts Y
                host_types:
                    a:
                        domains: X
                        arches: arch[123]
                    b:
                        domains: Y
                        arches: arch[23]
                    c:
                        domains: X|Y
                        arches:
                            - arch[3]
            """ + common_index_yaml,
            "output.txt.j2": OUTPUT_TXT_J2,
        }

        with assets_mkdir(assets_with_domains) as db_path:
            self.assertKpetProduces(
                kpet_with_db, db_path,
                "--debug", "run", "generate",
                "-t", "tree", "-a", "arch1",
                "-k", "kernel.tar.gz", "--no-lint",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST:TEST:Test1'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch1",
                stdout_equals='Test1\n'
            )

            self.assertKpetProduces(
                kpet_with_db, db_path,
                "--debug", "run", "generate",
                "-t", "tree", "-a", "arch2",
                "-k", "kernel.tar.gz", "--no-lint",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST:TEST:Test1'
                              'HOST:TEST:Test2'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch2",
                stdout_equals='Test1\nTest2\n'
            )

            self.assertKpetProduces(
                kpet_with_db, db_path,
                "--debug", "run", "generate",
                "-t", "tree", "-a", "arch3",
                "-k", "kernel.tar.gz", "--no-lint",
                stdout_equals='SCENE:RECIPESET:'
                              'HOST:TEST:Test1'
                              'HOST:TEST:Test2'
                              'HOST:TEST:Test3'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch3",
                stdout_equals='Test1\nTest2\nTest3\n'
            )

            self.assertKpetProduces(
                kpet_with_db, db_path,
                "--debug", "run", "generate",
                "-t", "tree", "-a", "arch4",
                "-k", "kernel.tar.gz", "--no-lint",
                stdout_equals='SCENE:'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch4",
                stdout_equals=''
            )

            self.assertKpetProduces(
                kpet_test_list, db_path,
                stdout_equals='Test1\nTest2\nTest3\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", ".*",
                stdout_equals='Test1\nTest2\nTest3\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch[12]",
                stdout_equals='Test1\nTest2\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch[23]",
                stdout_equals='Test1\nTest2\nTest3\n'
            )
            self.assertKpetProduces(
                kpet_test_list, db_path,
                "-a", "arch[34]",
                stdout_equals='Test1\nTest2\nTest3\n'
            )
